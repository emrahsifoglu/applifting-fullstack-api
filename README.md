# Applifting FullStack test api - STFUANDCLICK

### Technologies used

[Rails](http://rubyonrails.org/)

### Prerequisites

[RVM](https://rvm.io/)

### Installing

At first you need to set up database.yml for MySql.

``` yml

default: &default
  .
  username: [username]
  password: [password]
  .
  .

development:
  .
  database: [database]
```

Then you can run following command to create tables ```rails db:migrate```.

At last you may run ```rails db:seed``` to fill tables with dummy data.

## Running

You can simply run `rails s -p 4200`. Below you may find route list.

| Method  | Path                         | Params                        |
| ------  | ---------------------------  | ----------------------------- |
| GET     | [/api/v1/leaderboard](localhost:4200/api/v1/leaderboard)          |                               |
| GET     | /api/v1/:team/:session/kliks | team: string, session: string |
| POST    | /api/v1/klik                 | team: string, session: string |

## Authors

* **Emrah Sifoğlu** - *Initial work* - [emrahsifoglu](https://github.com/emrahsifoglu)

## License

This project is a task thus source is kept in a private repo.

Resources
========

- http://culttt.com/2015/12/09/defining-url-routes-in-ruby-on-rails/
- http://blog.bigbinary.com/2016/03/24/support-for-left-outer-joins-in-rails-5.html
- https://stackoverflow.com/questions/31222935/passing-params-to-sql-query
- https://stackoverflow.com/questions/3842818/how-to-change-rails-3-server-default-port-in-develoment
- https://til.hashrocket.com/posts/4d7f12b213-rails-5-api-and-cors
- https://til.hashrocket.com/posts/4d7f12b213-rails-5-api-and-cors
- https://github.com/cyu/rack-cors