class CreateLogs < ActiveRecord::Migration[5.1]
  def change
    create_table :logs do |t|
      t.string :session
      t.references :team, foreign_key: true
      t.integer :clicks

      t.timestamps
    end
  end
end
