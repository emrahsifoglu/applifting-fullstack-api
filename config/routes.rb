Rails.application.routes.draw do
  namespace 'api' do
    namespace 'v1' do
			get "leaderboard", to: "api#leaderBoard", as: "leaderboard"
			get ":team/:session/kliks", to: "api#kliks", as: "kliks"
			post "klik", to: "api#klik", as: "klik"
    end
  end
end
