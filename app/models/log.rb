class Log < ApplicationRecord
  belongs_to	:team
  validates 	:session, presence: true
	validates 	:clicks, presence: true
	scope				:teamTotalClicks, -> (limit) {
		joins(:team).
		select("sum(logs.clicks) as clicks, teams.name").
		group("logs.team_id").
		order("clicks DESC")
	}

	scope				:totalClicks, -> (team, session) {
		select("sum(logs.clicks) as your_clicks, team_log.team_clicks").
		joins("inner join (select sum(user_log.clicks) as team_clicks from logs user_log where user_log.team_id = #{team}) as team_log").
		where(team: team, session: session)
	}

	def self.leaders()
		order = 0 
		self.teamTotalClicks().map do |leader|
			order = order + 1 
			{ order: order, team: leader['name'], clicks: leader['clicks'] }
		end
	end

	def self.clickCounts(team, session)
		clickCounts = self.totalClicks(team, session)[0]
		{ your_clicks: clickCounts['your_clicks'], team_clicks: clickCounts['team_clicks'] }
	end

end
