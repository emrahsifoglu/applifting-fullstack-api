module Api
  module V1
    class ApiController < ApplicationController
			def errorOccurred(exception, status)
			  render json: { error: exception }.to_json, status: status
			end

      def leaderBoard
        leaders = Log.leaders()
        render json: leaders,status: :ok
      end

			def klik
				name = klikParams[:team]
				session = klikParams[:session]

				if (!session)
			  	return self.errorOccurred('RecordNotFound', 400)
				end

				team = self.getTeam(name)

				log = Log.new({ 
					session: session,
					team: team,
					clicks: 1
				})

				if !log.save
			  	return self.errorOccurred('saveError', 400)
				end

 				self.clickCounts(team.id, session)
			end

      def kliks
				team = klikParams[:team]
				session = klikParams[:session]
				self.clickCounts(team, session)
      end

      def getTeam(name)
      	team = Team.find_by({ name: name })
				if (!team)
						team = Team.new({ 
							name: name
						})
						if !team.save
			  			return self.errorOccurred('RecordNotFound', 400)
						end
				end

				return team
      end	

      def clickCounts(team, session)
        counts = Log.clickCounts(team, session)

        render json: counts,status: :ok
      end	

      def klikParams()
      	params.permit(:team, :session)
      end

    end
  end
end
