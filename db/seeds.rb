# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)


teams = Team.create([{ 
	name: 'Applifting'
},{ 
	name: 'Filip'
},{ 
	name: 'Mlask'
},{ 
	name: 'Jakub'
},{ 
	name: 'Prokop'
},{ 
	name: 'Zuzka'
},{ 
	name: 'Vrata'
},{ 
	name: 'Martin'
},{ 
	name: 'Borec'
},{ 
	name: 'CSP'
}])

100.times do
	logs = Log.create([{ 
		session: Faker::Lorem.characters(4) + '-' +Faker::Lorem.characters(18), 
		team_id: Faker::Number.between(1, 10),
		clicks: Faker::Number.between(1, 100)
	}])
end